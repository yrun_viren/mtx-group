// var data = [1, 2, 3];
// var number = 6;
removeRepeated([1, 2, 3], 0);
removeRepeated([1, 2, 2, 3, 3, 3, 4, 5, 5], 1);
removeRepeated([1, 2, 3], 6);

/**
 *
 * @param {array} data 
 * @param {number} number
 * @description It console the array which do not have the elements who repeated more than number value
 */
function removeRepeated(data, number) {
    var repeatedObj = {};
    var count = [];
    for (let i = 0; i < data.length; i++) {
        const element = data[i];
        if (repeatedObj[element]) {
            repeatedObj[element]++;
        } else {
            repeatedObj[element] = 1;
        }
    }

    for (const key in repeatedObj) {
        if (repeatedObj.hasOwnProperty(key)) {
            const element = repeatedObj[key];
            if (number >= element) {
                count.push(key);
            }
        }
    }
    console.log(count);
}